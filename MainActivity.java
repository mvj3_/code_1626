package com.wuhezhi.netimage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.wuhezhi.service.Service;

public class MainActivity extends Activity implements OnClickListener {
	private static final String PATH = "http://192.168.0.107:8080/MyWeb/map.png";
	private ImageView imageView;
	private Button button;
	private Bitmap bitmap;
	private ProgressDialog dialog;
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				Toast.makeText(MainActivity.this, "获取失败！", Toast.LENGTH_SHORT)
						.show();
				break;
			case 1:
				imageView.setImageBitmap(bitmap);
				break;
			default:
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();
		initLstener();
		dialog = new ProgressDialog(this);
		dialog.setMessage("正在获取中.....");
	}

	private void initView() {
		imageView = (ImageView) findViewById(R.id.iamgeView);
		button = (Button) findViewById(R.id.button);
	}

	private void initLstener() {
		button.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button:
			handler.post(new Runnable() {

				@Override
				public void run() {
					bitmap = getBitmap();
					if (bitmap != null) {
						handler.sendEmptyMessage(1);
					} else {
						handler.sendEmptyMessage(0);
					}
				}
			});
			break;
		default:
			break;
		}
	}

	private Bitmap getBitmap() {
		Bitmap bitmap = null;
		try {
			byte[] data = Service.getImage(PATH);
			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
}
